//
//  LocalizationManager.m
//  StaffDirectory
//
//  Created by Four Directions on 25/7/14.
//  Copyright (c) 2014 Four Directions. All rights reserved.
//

#import "LocalizationManager.h"

@implementation LocalizationManager

+ (LocalizationManager *)sharedManager {
    static dispatch_once_t pred;
    static LocalizationManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[LocalizationManager alloc] init];
    });
    return shared;
}

- (void)setLanguage:(NSString *)language
{
    NSString *path = [[ NSBundle mainBundle ] pathForResource:language ofType:@"lproj" ];
    if(!path)
    {
        path = [[ NSBundle mainBundle ] pathForResource:@"en" ofType:@"lproj" ];
    }
    bundle = [NSBundle bundleWithPath:path];
}

- (NSString *)localizedStringForKey:(NSString *)key
{
    return [bundle localizedStringForKey:key value:nil table:nil];
}

- (NSString *)localizedStringForKey:(NSString *)key language:(NSString *)language
{
    NSString *path = [[ NSBundle mainBundle ] pathForResource:language ofType:@"lproj" ];
    if(!path)
    {
        path = [[ NSBundle mainBundle ] pathForResource:@"en" ofType:@"lproj" ];
    }
    NSBundle *tempBundle = [NSBundle bundleWithPath:path];
    return [tempBundle localizedStringForKey:key value:nil table:nil];
}

@end
