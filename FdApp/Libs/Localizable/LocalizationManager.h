//
//  LocalizationManager.h
//  StaffDirectory
//
//  Created by Four Directions on 25/7/14.
//  Copyright (c) 2014 Four Directions. All rights reserved.
//

#import <Foundation/Foundation.h>

#define setLanguage(language) \
[[LocalizationManager sharedManager] setLanguage:(language)]

#define getLocalizedString(key) \
[[LocalizationManager sharedManager] localizedStringForKey:(key)]

#define getLocalizedStringWithLanguage(key,lang) \
[[LocalizationManager sharedManager] localizedStringForKey:(key) language:(lang)]

@interface LocalizationManager : NSObject
{
    NSBundle *bundle;
}

+ (LocalizationManager *)sharedManager;
- (void)setLanguage:(NSString *)language;
- (NSString *)localizedStringForKey:(NSString *)key;
- (NSString *)localizedStringForKey:(NSString *)key language:(NSString *)language;

@end
