//
//  Login.h
//  TeacherHall Mobile App
//
//  Created by Vincent on 2017/1/6.
//  Copyright © 2017年 吉祥具. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "loginUser.h"
@interface Login : NSObject
@property (nonatomic, assign) BOOL status;
@property (nonatomic, copy) NSString *firstname;
@property (nonatomic, copy) NSString *lastname;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *refCode;
@property (nonatomic, copy) NSString *memberID;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, assign) BOOL isTeacher;
@property (nonatomic, strong) loginUser *user;
@end
