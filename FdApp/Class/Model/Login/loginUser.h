//
//  loginUser.h
//  TeacherHall Mobile App
//
//  Created by Vincent on 2017/1/6.
//  Copyright © 2017年 吉祥具. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface loginUser : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *firstname;
@property (nonatomic, copy) NSString *lastname;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, assign) BOOL isTeacher;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *emailValidate;
@property (nonatomic, copy) NSString *emailValidateDate;
@property (nonatomic, copy) NSString *smsValidate;
@property (nonatomic, copy) NSString *smsValidateDate;
@property (nonatomic, copy) NSString *lang;
@property (nonatomic, copy) NSString *score;
@property (nonatomic, copy) NSString *link;
@end
