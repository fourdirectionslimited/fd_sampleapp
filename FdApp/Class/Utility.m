//
//  Utility.m
//  Freeplay
//
//  Created by Four Directions on 29/10/15.
//  Copyright © 2015 4d.com.HK. All rights reserved.
//

#import "Utility.h"
#import "Reachability.h"

@implementation Utility

+ (NSString *)getAppVersion {
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    version = [version stringByAppendingFormat:@"(%@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    return version;
}

+ (NSString *)getDeviceModel {
    return [UIDevice currentDevice].model;
}

+ (NSString *)getOsVersion {
    return [UIDevice currentDevice].systemVersion;
}

+ (NSString *)getUuid {
    //get vender id instead
    return [[[[[[UIDevice currentDevice].identifierForVendor.UUIDString description]
                                stringByReplacingOccurrencesOfString:@"<" withString:@""]
                               stringByReplacingOccurrencesOfString:@">" withString:@""]
                              stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""];
}

+ (BOOL)checkNetworkAvailable
{
    NetworkStatus status = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    if (status == NotReachable) {
        return NO;
    }
    return YES;
}

@end
