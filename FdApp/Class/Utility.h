//
//  Utility.h
//  Freeplay
//
//  Created by Four Directions on 29/10/15.
//  Copyright © 2015 4d.com.HK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+ (NSString *)getAppVersion;
+ (NSString *)getDeviceModel;
+ (NSString *)getOsVersion;
+ (NSString *)getUuid;
+ (BOOL)checkNetworkAvailable;

@end
