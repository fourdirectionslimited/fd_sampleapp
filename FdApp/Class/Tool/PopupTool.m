//
//  PopupTool.m
//  TradeReceipt
//
//  Created by Vincent on 2016/12/7.
//  Copyright © 2016年 Kit. All rights reserved.
//

#import "PopupTool.h"
#import <UIKit/UIKit.h>
@implementation PopupTool
+ (PopupTool *)sharePopupTool{
    
    static PopupTool*tool ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        tool = [[PopupTool alloc]init];
    });
    
    return tool;
}
- (instancetype)init{
    
    self = [super init];
    if (self) {
        // do anything you want
    }
    
    return self;
}


- (void)popuWithTitle:(NSString*)title  message:(NSString*)message isOnlyPopup:(BOOL)isOnlyPopup leftBtnMsg:(NSString*)leftBtnMsg  rightBtnMsg:(NSString*)rightBtnMsg  leftBtnHandler:(void (^)(void))leftBtnHandler   rightBtnHandler:(void (^)(void))rightBtnHandler viewController:(UIViewController*)viewController completion:(void (^)(void))completion{
    

    if (self.isPopuping&&isOnlyPopup) {
        
        NSLog(@"DXClog:%@", @"isPopuping");
        
        
        return;
    }else{
          self.isPopuping = YES;
    }
    
    UIViewController*popupViewController = viewController;
    if (popupViewController==nil) {
        
        
        popupViewController = [self getPresentedViewController];
        
        if (popupViewController==nil) {
            popupViewController = [self getCurrentVC];
        }
        
        
    }
    
    
    
    NSAssert(title!=nil||message!=nil, @"titile , 4dxm tips: message they should not be nil at the same time");
    
    UIAlertController*alertViewController = [UIAlertController alertControllerWithTitle:title   message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if (leftBtnMsg.length>0) {
        UIAlertAction*leftBtn = [UIAlertAction actionWithTitle:leftBtnMsg style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            self.isPopuping = NO;
            if (leftBtnHandler) {
                leftBtnHandler();
            }
            
        }];
        
        [alertViewController addAction:leftBtn];

    }
    
    if (rightBtnMsg.length>0) {
        UIAlertAction*rightAction = [UIAlertAction actionWithTitle:rightBtnMsg style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.isPopuping = NO;
            if (rightBtnHandler) {
                rightBtnHandler();
            }
            
        }];
        
        
        
        [alertViewController addAction:rightAction];

    }
    
    
    [popupViewController presentViewController:alertViewController animated:YES completion:^{
       
      
    }];
    
}

/**
 获取present出来的控制器

 @return <#return value description#>
 */
- (UIViewController *)getPresentedViewController
{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    if (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    
    return topVC;
}


/**
 获取当前控制器

 @return <#return value description#>
 */
- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}
@end
