//
//  PopupTool.h
//  TradeReceipt
//
//  Created by Vincent on 2016/12/7.
//  Copyright © 2016年 Kit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PopupTool : NSObject

+ (PopupTool*)sharePopupTool;
//properties
@property (nonatomic, assign) BOOL isPopuping;

/**
 popup window using UIAlertController

 @param title <#title description#>
 @param message <#message description#>
 @param isOnlyPopup Yes, it's the only popup window ,No there might be another popup pops up right after it pops up
                    yes,同一时间内只有这个popup window ,No, 在自己弹出后，还有可以让其他popup window 覆盖在上方，用户需要点击再次才能让所有popup 消失 
 
 @param leftBtnMsg <#leftBtnMsg description#>
 @param rightBtnMsg <#rightBtnMsg description#>
 @param leftBtnHandler <#leftBtnHandler description#>
 @param rightBtnHandler <#rightBtnHandler description#>
 @param viewController <#viewController description#>
 @param completion <#completion description#>
 */
- (void)popuWithTitle:(NSString*)title  message:(NSString*)message isOnlyPopup:(BOOL)isOnlyPopup leftBtnMsg:(NSString*)leftBtnMsg  rightBtnMsg:(NSString*)rightBtnMsg  leftBtnHandler:(void (^)(void))leftBtnHandler  rightBtnHandler:(void (^)(void))rightBtnHandler viewController:(UIViewController*)viewController completion:(void (^)(void))completion;
@end
