//
//  ViewControllerTool.m
//  FdApp
//
//  Created by Vincent on 2016/12/9.
//  Copyright © 2016年 Kit. All rights reserved.
//

#import "ViewControllerTool.h"

@implementation ViewControllerTool
+ (UIViewController*)topViewController{
    
    
    UIViewController*viewController = [self getPresentedViewController];
    if (viewController==nil) {
        
        return [self getCurrentVC];
    }else{
        return viewController;
    }
    
    
}

/**
 get present viewController
 
 @return <#return value description#>
 */
+ (UIViewController *)getPresentedViewController
{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    if (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    
    return topVC;
}


/**
 get current viewController
 
 @return <#return value description#>
 */
+ (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}
@end
