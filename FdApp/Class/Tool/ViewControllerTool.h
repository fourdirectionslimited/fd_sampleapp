//
//  ViewControllerTool.h
//  FdApp
//
//  Created by Vincent on 2016/12/9.
//  Copyright © 2016年 Kit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewControllerTool : NSObject

/**
 get a viewController used for popup window
 viewController

 @return <#return value description#>
 */
+ (UIViewController*)topViewController;
@end
