//
//  NavigationManager.h
//  Freeplay
//
//  Created by Kit on 15/10/30.
//  Copyright © 2015年 4d.com.HK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TestViewController.h"

@interface NavigationManager : NSObject<BaseViewControllerDelegate> {
    UINavigationController *nav;
}

+ (NavigationManager *)sharedManager;
- (UIViewController *)rootViewController;

@end
