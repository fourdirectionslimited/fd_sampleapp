//
//  AppSettingManager.m
//  Freeplay
//
//  Created by Four Directions on 29/10/15.
//  Copyright © 2015 4d.com.HK. All rights reserved.
//

#import "AppSettingManager.h"

#define key_userToken @"userToken"
#define key_userId @"userId"

@implementation AppSettingManager

+ (id)getSavedValue:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}

+ (void)saveValue:(NSObject *)object key:(NSString *)key {
    if(!object) {
        object = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getUserToken {
    return [self getSavedValue:key_userToken];
}

+ (void)setUserToken:(NSString *)userToken {
    [self saveValue:userToken key:key_userToken];
}

+ (NSString *)getUserId {
    return [self getSavedValue:key_userId];
}

+ (void)setUserId:(NSString *)userId {
    [self saveValue:userId key:key_userId];
}

@end
