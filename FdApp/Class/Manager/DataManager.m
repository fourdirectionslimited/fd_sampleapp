//
//  DataManager.m
//  Freeplay
//
//  Created by Four Directions on 29/10/15.
//  Copyright © 2015 4d.com.HK. All rights reserved.
//

#import "DataManager.h"
#import "CryptoManager.h"


@implementation DataManager

+ (DataManager *)sharedManager {
    static dispatch_once_t pred;
    static DataManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[DataManager alloc] init];
    });
    return shared;
}

-(id)init
{
    self = [super init];
    if (self) {
        //
    }
    return self;
}

+ (NSDictionary *)encryptData:(NSDictionary *)params{
    NSString* seedStr = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
    NSData * rsgisterParamsData = [NSJSONSerialization dataWithJSONObject:params
                                                                  options:NSJSONWritingPrettyPrinted
                                                                    error:nil];
    NSString * jsonStr = [[NSString alloc] initWithData:rsgisterParamsData encoding:NSUTF8StringEncoding];
    NSString * dataToPost = [[CryptoManager getInstance] encryptDataWithDataString:jsonStr  withSeed:seedStr];
    
    NSString *datastr = [NSString stringWithFormat:@"%@,%@",dataToPost,seedStr];
    NSMutableDictionary *dicRequsetObject = [[NSMutableDictionary alloc]init];
    [dicRequsetObject setObject:datastr forKey:@"data"];
    return dicRequsetObject;
}

+ (NSDictionary *)decryptData:(NSDictionary *)responseObject{
    NSString *allData = [responseObject objectForKey:@"data"];
    NSArray *allDataArr = [allData componentsSeparatedByString:@","];
    NSString *encodeData = [allDataArr firstObject];
    NSString *recSeedStr = [allDataArr lastObject];
    NSString * recStr = [[CryptoManager getInstance] decryptDataWithDataString:encodeData withSeed:recSeedStr];
    NSDictionary *dicResponseObject = [NSJSONSerialization JSONObjectWithData:
                                       [recStr dataUsingEncoding:NSUTF8StringEncoding]
                                                                      options:NSJSONReadingAllowFragments
                                                                        error:nil];
    return dicResponseObject;
}

@end
