//
//  DatabaseManager.m
//  Freeplay
//
//  Created by Four Directions on 29/10/15.
//  Copyright © 2015 4d.com.HK. All rights reserved.
//

#import "DatabaseManager.h"
//#import "FMDatabase.h"

#define databaseName @"database.sqlite"

@implementation DatabaseManager

+ (DatabaseManager *)sharedManager {
    static dispatch_once_t pred;
    static DatabaseManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[DatabaseManager alloc] init];
    });
    return shared;
}

/*- (id)init {
    self = [super init];
    if (self) {
        //copyDatabaseIfNotExist
        NSString *documentsDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:databaseName];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:path]) {
            NSString *sourcePath = [[NSBundle mainBundle] pathForResource:databaseName ofType:nil];
            NSError *error;
            [[NSFileManager defaultManager] copyItemAtPath:sourcePath
                                                    toPath:path
                                                     error:&error];
        }
        
        db = [[FMDatabase alloc] initWithPath:path];
        db.logsErrors = true;
    }
    return self;
}

- (void)open {
    [db open];
}


- (void)close {
    [db close];
}*/

@end
