//
//  AppSettingManager.h
//  Freeplay
//
//  Created by Four Directions on 29/10/15.
//  Copyright © 2015 4d.com.HK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSettingManager : NSObject

+ (NSString *)getUserToken;
+ (void)setUserToken:(NSString *)userToken;
+ (NSString *)getUserId;
+ (void)setUserId:(NSString *)userId;

@end
