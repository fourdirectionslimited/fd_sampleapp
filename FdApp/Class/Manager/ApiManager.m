//
//  ApiManager.m
//  FdApp
//
//  Created by Vincent on 2017/1/5.
//  Copyright © 2017年 Kit. All rights reserved.
//

#import "APIManager.h"
#import "Utility.h"
#import "AppSettingManager.h"
#import "DataManager.h"


@implementation ApiManager

+ (ApiManager *)sharedManager {
    static dispatch_once_t pred;
    static ApiManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[ApiManager alloc] init];
    });
    return shared;
}

-(id)init
{
    self = [super init];
    if (self) {
        _manager = [AFHTTPSessionManager manager];
        [_manager.requestSerializer setTimeoutInterval:30];
        
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",@"text/json",@"text/javascript",@"text/plain",@"text/xml", nil];
    }
    return self;
}



#pragma mark post api

/**
 regular api request
 
 all request would pop up a  loading pop-up ,if you don't need it ,delete MBProgressHUD instance manually
 loading pop-up dismisses after request finishes
 
 @param URLString       <#URLString description#>
 @param parameters      <#parameters description#>
 @param completeHandler <#completeHandler description#>
 @param failureHandler        <#failureHandler description#>
 */
- (void)handlePOST:(NSString *)URLString
        parameters:(id)parameters completeHandler:(completeHandler)completeHandler failureHandler:(failureHandler)failureHandler{
    
    
    UIView*topView= [ViewControllerTool topViewController].view;
    __block MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:topView animated:YES];
    
    [_manager POST:URLString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [hud hide:YES];
        
        completeHandler(responseObject);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [hud hide:YES];
        failureHandler(error);
        
        
    }];
    
    
}


/**
 file upload api request, include image and video
 
 
 @param URLString       <#URLString description#>
 @param parameters      <#parameters description#>
 @param block           file to be uploaded should be handle here
 {
 eg：
 upload a video：
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat =@"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.mp4", str];
 
    [formData appendPartWithFileData:file
    name:@"files[]"
    fileName:fileName
    mimeType:@"video/mp4"];
 
 eg:
 upload a image ：
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat =@"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
 
    [formData appendPartWithFileData:file
    name:@"files[]"
    fileName:fileName
    mimeType:@"image/jpeg"];
 
 
 }
 
 @param uploadProgress  <#uploadProgress description#>
 @param completeHandler <#completeHandler description#>
 @param failureHandler        <#failureHandler description#>
 */
- (void)handlePostWithURLString:(NSString *)URLString
                     parameters:(id)parameters
      constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                       progress:( void (^)(NSProgress * ))uploadProgress
                completeHandler:(completeHandler)completeHandler
                   failureHandler:(failureHandler)failureHandler
{
    UIView*topView= [ViewControllerTool topViewController].view;
    __block MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:topView animated:YES];
    
    
    [_manager POST:URLString parameters:parameters constructingBodyWithBlock:block progress:uploadProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [hud hide:YES];
        completeHandler(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [hud hide:YES];
        failureHandler(error);
    }];
    
    
};



#pragma mark handle api reqeust completion

/**
 handle all the  regular  api request completion in this api,
 
 every api should use the same status code 
 
 if you want to handle some apis independently ,
 
 then you use  (void)handleResponse:(id)responseObject withTag:(NSInteger) tag completeHandler:(completeHandler)completeHandler


 
 @param responseObject  <#responseObject description#>
 @param completeHandler <#completeHandler description#>
 */
- (void)handleResponse:(id)responseObject completeHandler:(completeHandler)completeHandler {
    
    
    // you may need to replace 'status' with  other wording
    if ([responseObject[@"status"] boolValue]) {
          completeHandler(responseObject);
    }else{
        
        //some other condition
        
        
    }
    
    completeHandler(responseObject);
//    NSLog(@"DXClog:%@", responseObject);
}


/**
 handle  regular api  request completion independently with tag
 
 @param responseObject  <#responseObject description#>
 @param tag             handle api independently
 @param completeHandler <#completeHandler description#>
 */
- (void)handleResponse:(id)responseObject withTag:(NSInteger) tag completeHandler:(completeHandler)completeHandler {
    
    
    switch (tag) {
            /*
             handle tag api independently
             case <#constant#>:
             <#statements#>
             break;
             
             
             */
        default:
            
//            NSLog(@"DXClog:%@", responseObject);
            
            break;
    }
    
    
    
}


#pragma mark handle api request failure

/**
 handle all regular api  request failure in this api
 
 it will pop up a message to ask whether you want to retry
 when errorcode  is
 NSURLErrorTimedOut or NSURLErrorNotConnectedToInternet
 
 
 @param urlString       <#urlString description#>
 @param parameters      <#parameters description#>
 @param error           <#error description#>
 @param failureHandler        <#failureHandler description#>
 @param completeHandler <#completeHandler description#>
 */
- (void)handleErrorWithURLString:(NSString*)urlString parameters:(NSDictionary*)parameters error:(NSError*)error failureHandler:(failureHandler)failureHandler completeHandler:(completeHandler)completeHandler {
    
    
    
    NSLog(@"networkerror:%@", error.localizedDescription);
    switch (error.code ) {
        case NSURLErrorTimedOut:{
            //pop up
            
            [[PopupTool sharePopupTool] popuWithTitle:getLocalizedString(@"PopupTitile") message:error.localizedDescription  isOnlyPopup:NO  leftBtnMsg:getLocalizedString(@"PopupCancel") rightBtnMsg:getLocalizedString(@"PopupRetry") leftBtnHandler:nil rightBtnHandler:^{
                
                [self handlePOST:urlString parameters:parameters completeHandler:^(id responseObject) {
                    
                    
                    
                    [self handleResponse:responseObject completeHandler:completeHandler];
                } failureHandler:^(NSError *error) {
                    [self handleErrorWithURLString:urlString parameters:parameters error:error failureHandler:failureHandler completeHandler:completeHandler];
                    
                }];
                
            } viewController:nil completion:nil];
            
        }
            break;
            
        case NSURLErrorNotConnectedToInternet:{
            //pop up
            [[PopupTool sharePopupTool] popuWithTitle:getLocalizedString(@"PopupTitile") message:getLocalizedString(@"PopupOutOfNetwork") isOnlyPopup:NO  leftBtnMsg:getLocalizedString(@"PopupCancel") rightBtnMsg:getLocalizedString(@"PopupRetry") leftBtnHandler:nil rightBtnHandler:^{
                
                [self handlePOST:urlString parameters:parameters completeHandler:^(id responseObject) {
                    
                    
                    
                    [self handleResponse:responseObject completeHandler:completeHandler];
                } failureHandler:^(NSError *error) {
                    [self handleErrorWithURLString:urlString parameters:parameters error:error failureHandler:failureHandler completeHandler:completeHandler];
                    
                }];
                
            } viewController:nil completion:nil];
            
        }
        default:
            //some other conditions
            
            break;
    }
    
}


/**
 
 handle all the file upload api request failure in this api
 
 it will pop up a message to ask whether you want to retry
 when errorcode  is
 NSURLErrorTimedOut or NSURLErrorNotConnectedToInternet
 
 @param urlString       <#urlString description#>
 @param parameters      <#parameters description#>
 @param block           <#block description#>
 @param uploadProgress  <#uploadProgress description#>
 @param error           <#error description#>
 @param failureHandler        <#failureHandler description#>
 @param completeHandler <#completeHandler description#>
 */
- (void)handleFileErrorWithURLString:(NSString*)urlString
                          parameters:(NSDictionary*)parameters
           constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                            progress:( void (^)(NSProgress * ))uploadProgress
                               error:(NSError*)error failureHandler:(failureHandler)failureHandler
                     completeHandler:(completeHandler)completeHandler {
    
    NSLog(@"networkerror:%@", error.localizedDescription);
    
    
    switch (error.code ) {
        case NSURLErrorTimedOut:{
            //pop up
            
            [[PopupTool sharePopupTool] popuWithTitle:getLocalizedString(@"PopupTitile") message:error.localizedDescription isOnlyPopup:NO    leftBtnMsg:getLocalizedString(@"PopupCancel") rightBtnMsg:getLocalizedString(@"PopupRetry")  leftBtnHandler:nil rightBtnHandler:^{
                
                
                [self handlePostWithURLString:urlString parameters:parameters constructingBodyWithBlock:block progress:uploadProgress completeHandler:^(id responseObject) {
                    
                    [self handleResponse:responseObject completeHandler:completeHandler];
                    
                } failureHandler:^(NSError *error) {
                    
                    [self handleFileErrorWithURLString:urlString parameters:parameters constructingBodyWithBlock:block progress:uploadProgress error:error failureHandler:failureHandler completeHandler:completeHandler];
                    
                    
                }];
                
                
            } viewController:nil completion:nil];
            
        }
            break;
            
        case NSURLErrorNotConnectedToInternet:{
            //pop up
            [[PopupTool sharePopupTool] popuWithTitle:getLocalizedString(@"PopupTitile") message:getLocalizedString(@"PopupOutOfNetwork") isOnlyPopup:NO  leftBtnMsg:getLocalizedString(@"PopupCancel") rightBtnMsg:getLocalizedString(@"PopupRetry") leftBtnHandler:nil rightBtnHandler:^{
                
                [self handlePostWithURLString:urlString parameters:parameters constructingBodyWithBlock:block progress:uploadProgress completeHandler:^(id responseObject) {
                    
                    [self handleResponse:responseObject completeHandler:completeHandler];
                    
                } failureHandler:^(NSError *error) {
                    
                    [self handleFileErrorWithURLString:urlString parameters:parameters constructingBodyWithBlock:block progress:uploadProgress error:error failureHandler:failureHandler completeHandler:completeHandler];
                    
                    
                }];
                
                
            } viewController:nil completion:nil];
            
        }
        default:
            //some other conditions
            
            break;
    }
    
}













/* ========================================================  API   ================================================================*/
#pragma mark  all the apis which would request server  should be written below here
//所有会声明在.h 文件里的方法在下方


#pragma mark   api request demo
/**
 regular api request demo
 
 @param completeHandler <#completeHandler description#>
 @param failureHandler        <#failureHandler description#>
 */
- (void)testWithEmail:(NSString*)email password:(NSString*)password  accessToken:(NSString*) accessToken completeHandler:(completeHandler)completeHandler  failureHandler:(failureHandler)failureHandler{
    
    
    
    NSDictionary*parameters = @{@"LoginForm[email]":email,@"LoginForm[password]":password,@"LoginForm[mobileToken]":@"LoginForm[mobileToken]"};
    
    NSString*url =[NSString stringWithFormat:testURL];
    
    [self handlePOST:url parameters:parameters completeHandler:^(id responseObject) {
        
        
        [self handleResponse:responseObject completeHandler:completeHandler];
        
    } failureHandler:^(NSError *error) {
        [self handleErrorWithURLString:url parameters:parameters error:error failureHandler:failureHandler completeHandler:completeHandler];
    }];
    
    
    
    
}

/**
 file upload demo
 
 @param accessToken     <#accessToken description#>
 @param block           <#block description#>
 @param uploadProgress  <#uploadProgress description#>
 @param completeHandler <#completeHandler description#>
 @param failureHandler        <#failureHandler description#>
 */
- (void)testUploadFileWtihToken:(NSString*)accessToken
      constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                       progress:( void (^)(NSProgress * ))uploadProgress
                completeHandler:(completeHandler)completeHandler
                   failureHandler:(failureHandler)failureHandler
{
    
    NSDictionary*parameters = @{@"accessToken":accessToken};
    
    
    NSString*url = [NSString stringWithFormat:fileUploadURL];
    
    [self handlePostWithURLString:url   parameters:parameters constructingBodyWithBlock:block progress:uploadProgress completeHandler:^(id responseObject) {
        
        [self handleResponse:responseObject completeHandler:completeHandler];
        
    } failureHandler:^(NSError *error) {
        
        [self handleFileErrorWithURLString:url parameters:parameters constructingBodyWithBlock:block progress:uploadProgress error:error failureHandler:failureHandler completeHandler:completeHandler];
        
    }];
}


@end
