//
//  DataManager.h
//  Freeplay
//
//  Created by Four Directions on 29/10/15.
//  Copyright © 2015 4d.com.HK. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Member;

@interface DataManager : NSObject

@property(strong,nonatomic) Member *loginUser;

+ (DataManager *)sharedManager;

+ (NSDictionary *)decryptData:(NSDictionary *)responseObject;
+ (NSDictionary *)encryptData:(NSDictionary *)params;

@end
