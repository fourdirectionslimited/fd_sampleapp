//
//  NavigationManager.m
//  Freeplay
//
//  Created by Kit on 15/10/30.
//  Copyright © 2015年 4d.com.HK. All rights reserved.
//

#import "NavigationManager.h"

@implementation NavigationManager

+ (NavigationManager *)sharedManager {
    static dispatch_once_t pred;
    static NavigationManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[NavigationManager alloc] init];
    });
    return shared;
}

-(id)init {
    self = [super init];
    if (self) {
        TestViewController *TestVc = [[TestViewController alloc] initWithNibName:@"TestViewController" bundle:nil];
        TestVc.delegate = self;
        nav = [[UINavigationController alloc] initWithRootViewController:TestVc];
        nav.navigationBarHidden = YES;
    }
    return self;
}

- (UIViewController *)rootViewController {
    return nav;
}

#pragma BaseViewControllerDelegate

- (void)onViewController:(BaseViewController *)viewController withEvent:(NSUInteger)event data:(id)data{
    
}

@end
