//
//  ApiManager.h
//  FdApp
//
//  Created by Vincent on 2017/1/5.
//  Copyright © 2017年 Kit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
@interface ApiManager : NSObject {
    NSMutableDictionary *generalParams;
    AFHTTPSessionManager *_manager;
}

typedef void(^failureHandler)(NSError *error);
typedef void(^completeHandler)(id responseObject);


+ (ApiManager *)sharedManager;



/**
 Test regular  api request
 
 @param Id <#Id description#>
 @param userid <#userid description#>
 @param accessToken <#accessToken description#>
 @param completeHandler <#completeHandler description#>
 @param errBlock <#errBlock description#>
 */
- (void)testWithEmail:(NSString*)email password:(NSString*)password  accessToken:(NSString*) accessToken completeHandler:(completeHandler)completeHandler  failureHandler:(failureHandler)failureHandler;




/**
 Test file(image ,video included) uploading api request

 @param accessToken <#accessToken description#>
 @param block <#block description#>
 @param uploadProgress <#uploadProgress description#>
 @param completeHandler <#completeHandler description#>
 @param errBlock <#errBlock description#>
 */
- (void)testUploadFileWtihToken:(NSString*)accessToken
      constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                       progress:( void (^)(NSProgress * ))uploadProgress
                completeHandler:(completeHandler)completeHandler
                 failureHandler:(failureHandler)failureHandler
;
@end
