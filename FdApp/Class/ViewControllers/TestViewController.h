//
//  RestrictTestViewController.h
//  Freeplay
//
//  Created by WanlinkGlobal on 15/10/13.
//  Copyright © 2015年 4d.com.HK. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum TestViewControllerEvent : NSUInteger {
    TestViewControllerEvent_Detail
} TestViewControllerEvent;

@protocol TestViewControllerDelegate <NSObject>

- (void)didTestViewControllerWithObject:(id)object event:(TestViewControllerEvent)event object:(id)object;

@end

@interface TestViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate> {
    __weak IBOutlet UITableView *testTableView;
}

@property(weak,nonatomic) id<TestViewControllerDelegate> delegate;

@end
