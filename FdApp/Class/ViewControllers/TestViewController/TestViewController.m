//
//  RestrictTestViewController.m
//  Freeplay
//
//  Created by WanlinkGlobal on 15/10/13.
//  Copyright © 2015年 4d.com.HK. All rights reserved.
//

#import "TestViewController.h"
#import "Login.h"
@interface TestViewController ()

@end

@implementation TestViewController

#pragma mark - viewMethods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //login model use with MJextension sample  
        [[ApiManager sharedManager] testWithEmail:@"VincentDeng@4dxm.com" password:@"11111111" accessToken:@"myToken" completeHandler:^(id responseObject) {
    
            Login*login = [Login mj_objectWithKeyValues:responseObject];
            
            NSLog(@"DXClog:%@--%@", login,login.user);
            
        } failureHandler:^(NSError *error) {
    
        }];
    
    
    /*
     
        image upload smaple
        NSData*fileData = UIImagePNGRepresentation([UIImage imageNamed:@"2.jpg"]);
    
        [[ApiManager sharedManager] testUploadFileWtihToken:@"" constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    
    
    
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat =@"yyyyMMddHHmmss";
            NSString *str = [formatter stringFromDate:[NSDate date]];
            NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
    
            //上传的参数(上传图片，以文件流的格式)
            [formData appendPartWithFileData:fileData
                                        name:@"files[]"
                                    fileName:fileName
                                    mimeType:@"image/jpeg"];
    
    
        } progress:^(NSProgress * _Nonnull prgress) {
    
    
            NSLog(@"DXClog1:all:%zd  now:%zd", prgress.totalUnitCount,prgress.completedUnitCount);
        } completeHandler:^(id responseObject) {
    
        } failureHandler:^(NSError *error) {
    
        }];
    
    
     video upload sample
     
    NSData*data = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"屏幕录制.mov" withExtension:nil]];
    
    
    
    [[ApiManager sharedManager] testUploadFileWtihToken:@"" constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat =@"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.mov", str];
        
        //上传的参数(上传图片，以文件流的格式)
        [formData appendPartWithFileData:data
                                    name:@"files[]"
                                fileName:fileName
                                mimeType:@"video/mov"];
        
        
    } progress:^(NSProgress * _Nonnull prgress) {
        
        
        NSLog(@"DXClog2:all:%zd  now:%zd", prgress.totalUnitCount,prgress.completedUnitCount);
    } completeHandler:^(id responseObject) {
        
    } failureHandler:^(NSError *error) {
        
    }];

     
    */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat yVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView].y;
    if (yVelocity < 0) {
        //scroll up
    } else if (yVelocity > 0) {
        //scroll down
    } else {
        //Can't determine direction as velocity is 0
    }
}

#pragma mark tableView dataSource & delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%d",(int)indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *data = [NSString stringWithFormat:@"%d",(int)(indexPath.row)];
//    [self.delegate didTestViewControllerWithObject:data event:TestViewControllerEvent_Detail ];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
