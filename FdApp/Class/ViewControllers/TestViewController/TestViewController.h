//
//  RestrictTestViewController.h
//  Freeplay
//
//  Created by WanlinkGlobal on 15/10/13.
//  Copyright © 2015年 4d.com.HK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TestViewController : BaseViewController <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate> {
    __weak IBOutlet UITableView *testTableView;
}


@end
