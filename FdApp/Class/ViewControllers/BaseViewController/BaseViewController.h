//
//  BaseViewController.h
//  FdApp
//
//  Created by Vincent on 2017/1/3.
//  Copyright © 2017年 Kit. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BaseViewController;
@protocol BaseViewControllerDelegate <NSObject>

-(void)onViewController:(BaseViewController *)viewController withEvent:(NSUInteger)event data:(id)data;

@end
@interface BaseViewController : UIViewController
@property (nonatomic, weak) id<BaseViewControllerDelegate> delegate;
@end
